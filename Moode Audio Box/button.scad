$fn=100;

cylHeight  = 6;
holeHeight = 3.2;
holeWidth  = 2.0;
holeDepth  = 3.0;
epsilon    = 0.1;

difference(){
translate([0,0,cylHeight/2.])
  cylinder(cylHeight,4,3.5,center=true);

translate([0,0,holeHeight/2.0-epsilon])
  cube([holeWidth,holeDepth,holeHeight],center=true);
}