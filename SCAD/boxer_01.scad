$fn=100;

//dxfFileName    = "../DXF/RPI_4_laser_cut_00.dxf";
//dxfFileName    = "../DXF/RPI_4_3D_00.dxf";
dxfFileName    = "../DXF/RPI_4_3D_01.dxf";
topLayer       = "BoxTop";
frontLayer     = "BoxFront";
backLayer      = "BoxBack";
sideLayer      = "BoxSide";
bottomLayer    = "BoxBottom";

layerNameVec = [topLayer,frontLayer,backLayer,sideLayer,bottomLayer];

epsilon              = 0.0;
nominalZ             = 3;
wallThickness        = nominalZ - epsilon;

/// TOP ///
module top(){
  linear_extrude(wallThickness)
    import(dxfFileName,layer=topLayer);
}

//// FRONT ///
module front(){
  linear_extrude(wallThickness)
    import(dxfFileName,layer=frontLayer);
}

//// BACK ///
module back(){
  linear_extrude(wallThickness)
    import(dxfFileName,layer=backLayer);
}

///  SIDES ////
module side(){
  linear_extrude(wallThickness)
    import(dxfFileName,layer=sideLayer);
}

module boxX(i,suff=""){
  xOffset = (i==4 ? 200 : 0 );
  ss = (i==4 ?"" : suff);
  echo(str(layerNameVec[i],suff));
  translate([xOffset,0,0])
    linear_extrude(wallThickness)
      import(dxfFileName,layer=str(layerNameVec[i],ss));
}

module all(small=false){
  suf = small ? "Small" : "";
  echo("toto",suf);
  for (i=[0:4])
    boxX(i,suf);
}
all(true);

moduleprintable(){
  projection(){
    union(){
      boxX(0);
      translate([-100,0,0])
        boxX(4);
      translate([90,70,0])
        boxX(1);
      translate([-80,70,0])
        boxX(2);
      translate([100,180,0])
        boxX(3);
    }
  }
}

module part(i){
  if(i==0)
    top();
  else if(i==1)
    front();
  else if(i==2)
    back();
  else if (i==3)
    side();
  else if (i==4)
    bottom();
}
