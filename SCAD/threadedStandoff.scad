include <threads.scad>
  
$fn=100;

dxfFileName = "../DXF/footClip.dxf";
clipLayer   = "Clip";
boxLayer    = "Box";
screwLayer  = "screwHole";


diam = 6; // 52 on first attempt
rad = diam/2.;
wall = 4;
outerR = rad+wall;
threadHt = 15;
innerHt  = 40;
//pit = 3;
//topHt =  8;


module boxSim(ht=12){
  linear_extrude(ht)
    import(dxfFileName,layer=boxLayer);
}
//boxSim(9);

module clip(ht=6){
  difference(){
    linear_extrude(ht)
      import(dxfFileName,layer=clipLayer);
    screwHole(ht);
  }
}
clip();

module screwHole(ht=10){
  translate([0,0,ht/2.])
    rotate([90,0,0])
    //translate([0,0,ht/2.])
      linear_extrude(ht*2.,center=true)  
        import(dxfFileName,layer=screwLayer);
}
//screwHole();

module threadedStandoff(ht,extD,intD){
  difference(){
    cylinder(d=extD,h=ht);
    cylinder(d=intD,h=ht);
    
    //metric_thread (diameter=intD, length=ht+2, internal=true);
  }
}

//threadedStandoff(10,6,2.6);

/*
module collar(){
 linear_extrude(innerHt)
    import (dxfFileName,layer= layers[0]);
  translate([0,0,-topHt])
    linear_extrude(topHt){
      import (dxfFileName,layer= layers[1]);
      import (dxfFileName,layer= layers[0]);
    }
  difference() {
    linear_extrude(threadHt)
      import (dxfFileName,layer= layers[1]);
    metric_thread (diameter=diam, pitch=pit, length=threadHt, internal=true);
  }
}
collar();

module ringBand(){
 linear_extrude(innerHt)
    import (dxfFileName,layer= layers[0]);
}
//ringBand();
*/