$fn=100;

dxfFileName    = "../DXF/RPI_4_original_updated.dxf";
topLayer       = "eBoxTop";
frontLayer     = "eBoxFront";
backLayer      = "eBoxBack";
sideLayer      = "eBoxSide";
sidePinLayer   = "ePinTab";
pinLayer       = "ePin";
clipLayer      = "eClip";
screwHoleLayer = "eScrewHole";

epsilon              = 1;
wallThickness        = 3;
tabResidualThickness = 0.85;
pinZ                 = 6;

/// TOP ///
module top(){
  linear_extrude(wallThickness)
    import(dxfFileName,layer=topLayer);
}

//// FRONT ///
module front(){
  linear_extrude(wallThickness)
    import(dxfFileName,layer=frontLayer);
}

//// BACK ///
module back(){
  linear_extrude(wallThickness)
    import(dxfFileName,layer=backLayer);
}

///  SIDES ////
module side(LorR){
  rotate([180,0,0])
    difference(){
      linear_extrude(wallThickness)
        import(dxfFileName,layer=str(sideLayer,LorR));
      linear_extrude(wallThickness-tabResidualThickness)
        import(dxfFileName,layer=str(sidePinLayer,LorR));
    }
}

////  PIN  //////

module pin(){
  linear_extrude(pinZ)
    import(dxfFileName,layer=pinLayer);
}

//// CLIP ////

module screwHole(ht=10){
  translate([0,0,ht/2.])
    rotate([90,0,0])
      linear_extrude(ht*2.,center=true)  
        import(dxfFileName,layer=screwHoleLayer);
}
//screwHole();

module clip(ht=pinZ){
  difference(){
    linear_extrude(ht)
      import(dxfFileName,layer=clipLayer);
    screwHole(ht);
  }
}

module part(i){
  if(i==0)
    top();
  else if(i==1)
    front();
  else if(i==2)
    back();
  else if (i==3)
    side("L");
  else if (i==4)
    side("R");
  else if (i== 5)
    clip();
  else if (i== 6)
    pin();
}

part(0);
/*
top();      // 0
front();    // 1
back();     // 2
side("L");  // 3
side("R");  // 4
clip();     // 5
pin();      // 6
*/