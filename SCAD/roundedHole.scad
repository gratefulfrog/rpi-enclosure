$fn = 100;

filetR          = 0.4;
holeDia         = 3.4;
holeR           = holeDia/2.;
holeL           = 10;
holePos         = [0,0,0];
holeOrientation = [0,0,0];

module lip(holeRad,filetRad){
  rotate_extrude()
    translate([holeRad+filetRad,0,0])
      circle(r=filetRad);
}


module test1(holeDia,roundDia,holeLen){
  hRr = holeDia/2.;
  fRr = roundDia/2;
  hHh = holeLen/2;
  bigR = hRr+roundDia;
  bigD = holeDia+roundDia;
  bigL = holeLen+roundDia;
  difference(){
    cylinder(r=bigR,h=bigL,center=true);
    union(){
      translate([0,0,hHh])
        lip(hRr,fRr);
      translate([0,0,-hHh])
        lip(hRr,fRr);
      difference(){
        cylinder(r=bigR,h=holeLen,center=true);
        cylinder(d=holeDia,h=holeLen,center=true);
      }
      difference(){
        cylinder(r=bigR,h=bigL,center=true);
        cylinder(d=bigD,h=bigL,center=true);
      }
    }
  }
}

difference(){
  cube([holeL+2*filetR,holeL+2*filetR,holeL+2*filetR],center=true);
  test1(holeDia,2*filetR,holeL);
}
